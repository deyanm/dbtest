package com.user.dbtest.utils;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;

import java.util.ArrayList;
import java.util.List;

public class LoginUtil {

    private static LoginUtil instance;

    private List<UserLogin> users = new ArrayList<>();
    private List<UserLogin> admins = new ArrayList<>();

    public static LoginUtil getInstance() {
        if (instance == null) {
            instance = new LoginUtil();
            instance.initUsers();
        }

        return instance;
    }

    private void initUsers() {
        users.add(new UserLogin("Ivan", "123456"));

        admins.add(new UserLogin("admin", "admin"));
    }

    public boolean checkUserLogin(UserLogin userLogin){
        if (users.contains(userLogin))
            return true;
        return admins.contains(userLogin);
    }

    public boolean isAdmin(final String username){
        return Stream.of(admins).anyMatch(new Predicate<UserLogin>() {
            @Override
            public boolean test(UserLogin user) {
                return user.getUsername().equals(username);
            }
        });
    }
}
