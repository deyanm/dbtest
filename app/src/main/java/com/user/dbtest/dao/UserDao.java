package com.user.dbtest.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.user.dbtest.model.UserBean;

import java.util.List;

@Dao
public interface UserDao {

    @Insert
    void insertUserdb(UserBean userBean);

    @Update
    void updateUserdb(UserBean userBean);

    @Delete
    void deleteUserdb(UserBean userBean);

    @Query("SELECT * FROM userBean")
    List<UserBean> getAllUserBeans();


}
