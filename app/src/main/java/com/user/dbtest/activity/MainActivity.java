package com.user.dbtest.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.user.dbtest.R;
import com.user.dbtest.config.AppDbConnection;
import com.user.dbtest.dao.UserDao;
import com.user.dbtest.model.UserBean;
import com.user.dbtest.utils.LoginUtil;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edtName, edtPhone, edtEmail, edtAdress, edtId, edtFacultyNumber, edtCourse;
    Button btnSubmit, btnView, btnLogout;

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);


    SharedPreferences pref;

    private List<UserBean> userBeanList;
    private UserBean userBeanFile;
    private UserDao userDaoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        pref = getApplicationContext().getSharedPreferences("testPreferences", MODE_PRIVATE);

        edtName = findViewById(R.id.edtName);
        edtPhone = findViewById(R.id.edtPhone);
        edtEmail = findViewById(R.id.edtEmail);
        edtAdress = findViewById(R.id.edtAddress);
        edtId = findViewById(R.id.edtId);
        edtFacultyNumber = findViewById(R.id.edtFacultyNumber);
        edtCourse = findViewById(R.id.edtCourse);


        btnSubmit = findViewById(R.id.btnSubmit);
        btnView = findViewById(R.id.btnView);
        btnLogout = findViewById(R.id.btn_logout);
        btnSubmit.setOnClickListener(this);
        btnView.setOnClickListener(this);
        btnLogout.setOnClickListener(this);


        if (!LoginUtil.getInstance().isAdmin(pref.getString("username", ""))) {
            btnView.setVisibility(View.GONE);
        }

        AppDbConnection appDbConnection = Room.databaseBuilder(this, AppDbConnection.class, "user_db")
                .allowMainThreadQueries()
                .build();

        userDaoFile = appDbConnection.userDao();

        Intent intent = getIntent();
        userBeanFile = intent.getParcelableExtra("myParcebaleData");
        if (userBeanFile != null) {


            btnSubmit.setText("Update");
            edtId.setText(userBeanFile.getId() + "");
            edtName.setText(userBeanFile.getName());
            edtPhone.setText(userBeanFile.getPhone() + "");
            edtEmail.setText(userBeanFile.getEmail());
            edtAdress.setText(userBeanFile.getAddress());
            edtCourse.setText(userBeanFile.getCourse() + "");
            edtFacultyNumber.setText(userBeanFile.getFaculty_number() + "");

        }


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSubmit) {
            if (checkInputs()) {
                Toast.makeText(this, "Please fill all fields ", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!checkMailFormat()) {
                Toast.makeText(this, "Please enter valid mail address ", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!checkPhoneNumber()) {
                Toast.makeText(this, "Please enter phone number ", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!checkCourse()) {
                Toast.makeText(this, "Please enter valid course information ", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!checkFacultyNumber()) {
                Toast.makeText(this, "Please enter valid faculty number ", Toast.LENGTH_SHORT).show();
                return;
            }

            if (userBeanFile == null) {
                userBeanFile = new UserBean();
                userBeanFile.setName(edtName.getText().toString());
                userBeanFile.setEmail(edtEmail.getText().toString());
                userBeanFile.setAddress(edtAdress.getText().toString());
                userBeanFile.setPhone(Integer.parseInt(edtPhone.getText().toString()));
                userBeanFile.setFaculty_number(Integer.parseInt(edtFacultyNumber.getText().toString()));
                userBeanFile.setCourse(Integer.parseInt(edtCourse.getText().toString()));


                userDaoFile.insertUserdb(userBeanFile);
                Toast.makeText(this, "Name : " + userBeanFile.getName() + "\n" +
                        "Email : " + userBeanFile.getEmail() + "\n" +
                        "Address: " + userBeanFile.getAddress(), Toast.LENGTH_LONG).show();
            } else {
                userBeanFile.setName(edtName.getText().toString());
                userBeanFile.setEmail(edtEmail.getText().toString());
                userBeanFile.setAddress(edtAdress.getText().toString());
                userBeanFile.setPhone(Integer.parseInt(edtPhone.getText().toString()));
                userBeanFile.setFaculty_number(Integer.parseInt(edtFacultyNumber.getText().toString()));
                userBeanFile.setCourse(Integer.parseInt(edtCourse.getText().toString()));

                userDaoFile.updateUserdb(userBeanFile);
                Toast.makeText(this, "Data Updated Successfully ", Toast.LENGTH_SHORT).show();
            }
            resetAllData(v);

        }
        if (v.getId() == R.id.btnView) {

            Intent intent = new Intent(this, DetailActivity.class);
            startActivity(intent);
        }

        if (v.getId() == R.id.btn_logout) {
            pref.edit().clear().apply();
            Intent i = new Intent(this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }

    private void resetAllData(View v) {
        edtId.setText("");
        edtId.setVisibility(View.GONE);
        edtName.setText("");
        edtPhone.setText("");
        edtEmail.setText("");
        edtAdress.setText("");
        edtFacultyNumber.setText("");
        edtCourse.setText("");
        btnSubmit.setText("Submit");

        userBeanFile = null;
    }

    private boolean checkInputs() {
        return TextUtils.isEmpty(edtName.getText().toString())
                || TextUtils.isEmpty(edtEmail.getText().toString())
                || TextUtils.isEmpty(edtAdress.getText().toString())
                || TextUtils.isEmpty(edtCourse.getText().toString())
                || TextUtils.isEmpty(edtFacultyNumber.getText().toString())
                || TextUtils.isEmpty(edtPhone.getText().toString());
    }

    private boolean checkMailFormat(){
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(edtEmail.getText().toString());
        return matcher.find();
    }

    private boolean checkCourse(){
        return Integer.parseInt(edtCourse.getText().toString()) < 10;
    }

    private boolean checkFacultyNumber(){
        return edtFacultyNumber.getText().toString().length() == 9;
    }

    public boolean checkPhoneNumber(){
        String phone = edtPhone.getText().toString();

        return (phone.startsWith("0") && phone.length() == 10)
                || (phone.startsWith("359") && phone.length() == 12);
    }
}
