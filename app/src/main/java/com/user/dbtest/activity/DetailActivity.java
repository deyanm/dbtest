package com.user.dbtest.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.user.dbtest.R;
import com.user.dbtest.adapter.UserListCustomAdapter;
import com.user.dbtest.config.AppDbConnection;
import com.user.dbtest.dao.UserDao;
import com.user.dbtest.model.UserBean;


import java.util.List;

public class DetailActivity extends AppCompatActivity implements UserListCustomAdapter.OnUserClickListner  {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    private List<UserBean> listusers;
    private  UserBean userBeanFile;
    private UserDao userDaoFile;
    UserListCustomAdapter userListCustomAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        recyclerView=findViewById(R.id.listUser);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        AppDbConnection appDatabaseCon= Room.databaseBuilder(this,AppDbConnection.class,"user_db")
                .allowMainThreadQueries()
                .build();

        userDaoFile=appDatabaseCon.userDao();
        listusers=userDaoFile.getAllUserBeans();

        userListCustomAdapter= new UserListCustomAdapter(listusers,this);
        recyclerView.setAdapter(userListCustomAdapter);

    }


    @Override
    public void onUserClick(UserBean userBean, String myAction) {
        if(myAction.equals("Delete")){
            userDaoFile.deleteUserdb(userBean);
            userListCustomAdapter.notifyDataSetChanged();
            Toast.makeText(this, userBean.getName()+" Is Deleted", Toast.LENGTH_SHORT).show();
        }
        if(myAction.equals("Edit")){

            Intent intent=new Intent(DetailActivity.this, MainActivity.class);
            intent.putExtra("myParcebaleData",userBean);
            startActivity(intent);
            this.finish();
        }
    }
}
