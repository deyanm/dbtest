package com.user.dbtest.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.user.dbtest.R;
import com.user.dbtest.utils.LoginUtil;
import com.user.dbtest.utils.UserLogin;

import org.w3c.dom.Text;

public class LoginActivity extends AppCompatActivity {

    SharedPreferences pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        pref = getApplicationContext().getSharedPreferences("testPreferences", MODE_PRIVATE);

        checkIsLogged();

        final Button loginButton = findViewById(R.id.btnLogin);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void login() {
        EditText username = findViewById(R.id.edtUsername);
        EditText password = findViewById(R.id.edtPassword);
        if (TextUtils.isEmpty(username.getText().toString()) || TextUtils.isEmpty(password.getText().toString())) {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show();
            return;
        }

        UserLogin userLogin = new UserLogin(username.getText().toString(), password.getText().toString());

        if (LoginUtil.getInstance().checkUserLogin(userLogin)) {
            pref.edit()
                    .putString("username", userLogin.getUsername())
                    .apply();

            Intent i = new Intent(this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else {
            Toast.makeText(this, "Wong credential", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkIsLogged() {
        if (!TextUtils.isEmpty(pref.getString("username", ""))) {
            Intent i = new Intent(this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }
}
