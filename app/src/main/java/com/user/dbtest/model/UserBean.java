package com.user.dbtest.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class UserBean implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    int id;

    int phone;
    String email, name, address;
    int faculty_number, course;


    public UserBean() {

    }

    protected UserBean(Parcel in) {
        id = in.readInt();
        phone = in.readInt();
        email = in.readString();
        name = in.readString();
        address = in.readString();
        faculty_number = in.readInt();
        course = in.readInt();
    }

    public static final Creator<UserBean> CREATOR = new Creator<UserBean>() {
        @Override
        public UserBean createFromParcel(Parcel in) {
            return new UserBean(in);
        }

        @Override
        public UserBean[] newArray(int size) {
            return new UserBean[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getFaculty_number() {
        return faculty_number;
    }

    public void setFaculty_number(int faculty_number) {
        this.faculty_number = faculty_number;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "id=" + id +
                ", phone=" + phone +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", faculty_number='" + faculty_number + '\'' +
                ", course='" + course + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(phone);
        dest.writeString(email);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeInt(faculty_number);
        dest.writeInt(course);
    }
}
